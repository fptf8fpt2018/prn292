﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesWebsite.Controllers
{
    public class DemoCssController : Controller
    {
        public ActionResult Demo()
        {
            return View();
        }

        public ActionResult DemoLogin()
        {
            return View();
        }

        public ViewResult DemoLoginWithHTML5()
        {
            return View();
        }
    }
}