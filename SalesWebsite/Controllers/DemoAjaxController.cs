﻿using SalesWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesWebsite.Controllers
{
    public class DemoAjaxController : Controller
    {
        // GET: DemoAjax
        public ActionResult DemoAjax()
        {
            return View();
        }



        [HttpPost]
        public ActionResult MethodForAjaxActionLinkByTrieuDD_InServer()
        {
            System.Threading.Thread.Sleep(2000);
            return Content("TrieuDD dep troai\n");
        }

        [HttpPost]
        public ActionResult MethodForAjaxBeginFormByTrieuDD_InServer(FormCollection formCollection)
        {
            String a = formCollection["txtTrieuDD"];
            return Content(formCollection.Get("txtTrieuDD"));
        }

        [HttpGet]
        public ActionResult MethodForAjaxJqueryByTrieuDD_InServer(int a, int b)
        {
            return Content("TrieuDD dep troai " + (a + b) + " \n");
        }

        MyUpdateBusinessEntities allDAO = new MyUpdateBusinessEntities();
     
        public ActionResult MethodForAjaxActionLinkReturnPartialViewByTrieuDD_InServer()
        {
            var producst = from trieudd in allDAO.Products
                           select trieudd;
            return PartialView(producst);
        }
    }
}