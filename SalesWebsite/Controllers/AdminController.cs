﻿using SalesWebsite.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesWebsite.Controllers
{
    public class AdminController : Controller
    {
        MyUpdateBusinessEntities allDAO = new MyUpdateBusinessEntities();
        public ActionResult Index()
        {
            return View(allDAO.Products.Where(trieudd => trieudd.is_delete == false));
        }

        [HttpGet]
        public ActionResult AddNew()
        {
            ViewBag.Providers = new SelectList(allDAO.Providers.OrderBy(trieudd => trieudd.name), "id", "name");
            ViewBag.ProductTypes = new SelectList(allDAO.ProductTypes.OrderBy(trieudd => trieudd.id), "id", "name");
            ViewBag.Producers = new SelectList(allDAO.Producers.OrderBy(trieudd => trieudd.name), "id", "name");
            return View(new Product());
        }
        [HttpGet]
        public ActionResult AddNew1()
        {
            ViewBag.Providers = new SelectList(allDAO.Providers.OrderBy(trieudd => trieudd.name), "id", "name");
            ViewBag.ProductTypes = new SelectList(allDAO.ProductTypes.OrderBy(trieudd => trieudd.id), "id", "name");
            ViewBag.Producers = new SelectList(allDAO.Producers.OrderBy(trieudd => trieudd.name), "id", "name");
            return View(new Product());
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddNew1(Product product, HttpPostedFileBase[] uploadFile)
        {
            // load again combobox
            ViewBag.Providers = new SelectList(allDAO.Providers.OrderBy(trieudd => trieudd.name), "id", "name");
            ViewBag.ProductTypes = new SelectList(allDAO.ProductTypes.OrderBy(trieudd => trieudd.id), "id", "name");
            ViewBag.Producers = new SelectList(allDAO.Producers.OrderBy(trieudd => trieudd.name), "id", "name");

            // check image is exist
            if (uploadFile[0].ContentLength > 0)
            {
                var path = Path.Combine(Server.MapPath("~/Content/imagesProducts"), Path.GetFileName(uploadFile[0].FileName));

                if (System.IO.File.Exists(path))
                {
                    ViewBag.Msg = "This image is exist";
                    return View(new Product() { name = "TrieuDZ" });
                }
                else
                {
                    uploadFile[0].SaveAs(path);
                    product.image = uploadFile[0].FileName;
                }
            }
            allDAO.Products.Add(product);
            allDAO.SaveChanges();
            return RedirectToAction("Index");

        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddNew(Product product, HttpPostedFileBase[] uploadFile)
        {
            // load again combobox
            ViewBag.Providers = new SelectList(allDAO.Providers.OrderBy(trieudd => trieudd.name), "id", "name");
            ViewBag.ProductTypes = new SelectList(allDAO.ProductTypes.OrderBy(trieudd => trieudd.id), "id", "name");
            ViewBag.Producers = new SelectList(allDAO.Producers.OrderBy(trieudd => trieudd.name), "id", "name");

            // check image is exist
            if (uploadFile[0].ContentLength > 0)
            {
                var path = Path.Combine(Server.MapPath("~/Content/imagesProducts"), Path.GetFileName(uploadFile[0].FileName));

                if (System.IO.File.Exists(path))
                {
                    ViewBag.Msg = "This image is exist";
                    return View(new Product() { name = "TrieuDZ" });
                }
                else
                {
                    uploadFile[0].SaveAs(path);
                    product.image = uploadFile[0].FileName;
                }
            }
            allDAO.Products.Add(product);
            allDAO.SaveChanges();
            return RedirectToAction("Index");

        }

        [HttpGet]
        public ActionResult UpdateProduct(int? id)
        {
            //no pass value of parameter -- không truyền id
            if (id == null)
            {
                Response.StatusCode = 404;
                return null;
            }

            //
            Product product = allDAO.Products.SingleOrDefault(trieudd => trieudd.id == id);
            if (product == null)
            {
                return HttpNotFound();
            }




            // load again combobox
            ViewBag.Providers = new SelectList(allDAO.Providers.OrderBy(trieudd => trieudd.name), "id", "name", product.provider_id);
            ViewBag.ProductTypes = new SelectList(allDAO.ProductTypes.OrderBy(trieudd => trieudd.id), "id", "name", product.product_type_id);
            ViewBag.Producers = new SelectList(allDAO.Producers.OrderBy(trieudd => trieudd.name), "id", "name", product.producer_id);

            return View(product);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult UpdateProduct(Product product)
        {
            //// load again combobox
            //ViewBag.Providers = new SelectList(allDAO.Providers.OrderBy(trieudd => trieudd.name), "id", "name", product.provider_id);
            //ViewBag.ProductTypes = new SelectList(allDAO.ProductTypes.OrderBy(trieudd => trieudd.id), "id", "name", product.product_type_id);
            //ViewBag.Producers = new SelectList(allDAO.Producers.OrderBy(trieudd => trieudd.name), "id", "name", product.producer_id);


            //if (ModelState.IsValid)
            //{
                allDAO.Entry(product).State = System.Data.Entity.EntityState.Modified;
            allDAO.SaveChanges();
            return RedirectToAction("Index");
            //}


            // load again combobox
            ViewBag.Providers = new SelectList(allDAO.Providers.OrderBy(trieudd => trieudd.name), "id", "name");
            ViewBag.ProductTypes = new SelectList(allDAO.ProductTypes.OrderBy(trieudd => trieudd.id), "id", "name");
            ViewBag.Producers = new SelectList(allDAO.Producers.OrderBy(trieudd => trieudd.name), "id", "name");
            return View(product);
        }
    }
}