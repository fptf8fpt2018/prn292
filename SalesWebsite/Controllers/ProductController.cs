﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SalesWebsite.Models;
using PagedList;
namespace SalesWebsite.Controllers
{
    public class ProductController : Controller
    {
        MyUpdateBusinessEntities allDAO = new MyUpdateBusinessEntities();

        // display style 1
        [ChildActionOnly]
        public ActionResult ProductStyle1Parital()
        {
            return PartialView();
        }

        // display style 2
        [ChildActionOnly]
        public ActionResult ProudctStyle2Partial()
        {
            return PartialView();
        }

        //see detail of product by id passed into
        public ActionResult DetailProduct(int? idd)
        {
            if (idd == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //find product by id and !isDelete ------> return product^null
            var product = allDAO.Products.SingleOrDefault(trieudd => trieudd.id == idd && trieudd.is_delete == false);
            if (product == null)
                return HttpNotFound();
            return View(product);
        }

        public ActionResult ProductsByTypeAndProvider(int? typeId, int? producerId, int? page)
        {

            if (typeId == null || producerId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var products = allDAO.Products.Where(trieudd => trieudd.product_type_id == typeId && trieudd.producer_id == producerId);
            if (products.Count() == 0)
            {
                return HttpNotFound();
            }

            // pagination
            if (Request.HttpMethod != "GET")
            {
                page = 1;
            }
            int pageSize = 6;
            int pageNumber = (page ?? 1);

            ViewBag.TypeID = typeId;
            ViewBag.ProducerID = producerId;
            return View(products.OrderBy(trieudd => trieudd.id).ToPagedList(pageNumber, pageSize));
        }
    }
}