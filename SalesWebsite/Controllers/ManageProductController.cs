﻿using SalesWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesWebsite.Controllers
{
    public class ManageProductController : Controller
    {
        MyUpdateBusinessEntities allDAO = new MyUpdateBusinessEntities();
        public ActionResult Index()
        {
            return View(allDAO.Products.Where(trieudd => trieudd.is_delete == false));
        }
    }
}