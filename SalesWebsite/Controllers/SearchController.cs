﻿using SalesWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList.Mvc;
using PagedList;

namespace SalesWebsite.Controllers
{
    public class SearchController : Controller
    {
        MyUpdateBusinessEntities allDAO = new MyUpdateBusinessEntities();

        [HttpGet]
        public ActionResult SearchResult(string key, int? page)
        {
            if (Request.HttpMethod != "GET")
            {
                page = 1;
            }
            int pageNumber = (page ?? 1);
            int pageSize = 6;
            var products = allDAO.Products.Where(trieudd => trieudd.name.Contains(key));
            ViewBag.Key = key;
            return View(products.OrderBy(trieudd => trieudd.name).ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public ActionResult SearchResult(string key)
        {
            ////if (Request.HttpMethod != "GET")
            ////{
            ////    page = 1;
            ////}
            ////int pageNumber = (page ?? 1);
            ////int pageSize = 6;
            ////var products = allDAO.Products.Where(trieudd => trieudd.name.Contains(key));
            ////ViewBag.Key = key;
            //return View(products.OrderBy(trieudd => trieudd.name).ToPagedList(pageNumber, pageSize));

            // muc dich de co tu khoa tren url va ngan gon code
            return RedirectToAction("SearchResult", new { @key = key });
        }


        public ActionResult SearchResultPartial(string key)
        {
            var products = allDAO.Products.Where(trieudd => trieudd.name.Contains(key));
            ViewBag.Key = key;
            return PartialView(products.OrderBy(trieudd => trieudd.price));
        }
    }
}