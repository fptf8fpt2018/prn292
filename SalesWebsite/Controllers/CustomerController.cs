﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using SalesWebsite.Models;

namespace SalesWebsite.Controllers
{
    public class CustomerController : Controller
    {
        MyUpdateBusinessEntities allDAO = new MyUpdateBusinessEntities();

        // GET: Customer
        public ActionResult Index()
        {
            var lstOfCus = allDAO.Customers;
            return View(lstOfCus);
        }

        public ActionResult Index1()
        {
            var lstOfCus = from all in allDAO.Customers select all;
            return View(lstOfCus);
        }

        public ViewResult ReadFirstt()
        {

            return View(allDAO.Customers.FirstOrDefault());
        }

        public ViewResult SearchByID()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SearchByID(FormCollection formCollection)
        {
            int id = int.Parse(formCollection.Get("txtID").Trim());
            var customers = from customer in allDAO.Customers
                            where customer.id == id
                            select customer;
            Customer aCustomer = customers.FirstOrDefault();

            return View(aCustomer);
        }


        public ActionResult SearchByID1()
        {
            Customer customer = allDAO.Customers.SingleOrDefault(trieudd => trieudd.id == 3);

            return View(customer);
        }

        public ViewResult Sort()
        {
            //  List<Customer> customers = allDAO.Customers.OrderByDescending(ahihi => ahihi.id).ToList();
            var customers = from cus in allDAO.Customers
                            orderby cus.id
                            select cus;
            return View(customers);
        }

        public ViewResult Group()
        {
            return View(allDAO.Members.OrderByDescending(ahuhu => ahuhu.member_type_id).ToList());
        }
    }
}