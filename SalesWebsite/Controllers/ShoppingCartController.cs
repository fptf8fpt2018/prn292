﻿using SalesWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesWebsite.Controllers
{
    public class ShoppingCartController : Controller
    {
        MyUpdateBusinessEntities allDAO = new MyUpdateBusinessEntities();

        [ChildActionOnly]
        public ActionResult ShoppingCartPartial()
        {
            ViewBag.SumOfAmount = GetSumOfAmount() != 0 ? GetSumOfAmount() : 0;
            ViewBag.SumOfAllBills = GetSumOfAmount() == 0 ? 0 : GetSumOfAllBills();
            return PartialView();
        }


        // create session

        public List<ShoppingCartItem> GetShoppingCart()
        {
            List<ShoppingCartItem> shoppingCartItems = Session["shoppingCartItemsByTrieuDD"] as List<ShoppingCartItem>;
            if (shoppingCartItems == null)
            {
                shoppingCartItems = new List<ShoppingCartItem>();
                Session["shoppingCartItemsByTrieuDD"] = shoppingCartItems;
            }
            return shoppingCartItems;
        }
        public ActionResult WatchShoppingCart()
        {
            List<ShoppingCartItem> shoppingCartItems = GetShoppingCart();
            return View(shoppingCartItems);
        }
        // for display in cart by session
        public int GetSumOfAmount()
        {
            List<ShoppingCartItem> shoppingCartItems = Session["shoppingCartItemsByTrieuDD"] as List<ShoppingCartItem>;
            if (shoppingCartItems == null)
            {
                return 0;
            }
            return shoppingCartItems.Sum(trieudd => trieudd.Amount);
        }
        public decimal GetSumOfAllBills()
        {
            List<ShoppingCartItem> shoppingCartItems = Session["shoppingCartItemsByTrieuDD"] as List<ShoppingCartItem>;
            if (shoppingCartItems == null)
            {
                return 0;
            }
            return shoppingCartItems.Sum(trieudd => trieudd.Bill);
        }
        public ActionResult AddShoppingCart(int id, string url)
        {
            // check in db
            Product product = allDAO.Products.SingleOrDefault(trieudd => trieudd.id == id);
            if (product == null)
            {
                Response.StatusCode = 400;
                return null;
            }

            // check in shopping cart
            List<ShoppingCartItem> shoppingCartItems = GetShoppingCart();
            ShoppingCartItem shoppingCartItem = shoppingCartItems.SingleOrDefault(trieudd => trieudd.ProductID == id);


            if (shoppingCartItem == null)
            {
                shoppingCartItem = new ShoppingCartItem(id);
                if (product.inventory < shoppingCartItem.Amount)
                {
                    return View("Notification");
                }
                else
                {
                    shoppingCartItems.Add(shoppingCartItem);
                }
            }
            else
            {
                if (product.inventory < shoppingCartItem.Amount + 1)
                {
                    return View("Notification");
                }
                else
                {
                    shoppingCartItem.Amount++;
                    shoppingCartItem.Bill = shoppingCartItem.Amount * shoppingCartItem.Price;
                }
            }
            return Redirect(url);
        }
        public ActionResult AddShoppingCartAjax(int id)
        {
            // check in db
            Product product = allDAO.Products.SingleOrDefault(trieudd => trieudd.id == id);
            if (product == null)
            {
                Response.StatusCode = 400;
                return null;
            }

            // check in shopping cart
            List<ShoppingCartItem> shoppingCartItems = GetShoppingCart();
            ShoppingCartItem shoppingCartItem = shoppingCartItems.SingleOrDefault(trieudd => trieudd.ProductID == id);


            if (shoppingCartItem == null)
            {
                shoppingCartItem = new ShoppingCartItem(id);
                if (product.inventory < shoppingCartItem.Amount)
                {
                    return Content("<script>alert('This product sold all !!!');window.location.reload();</script>");
                }
                else
                {
                    shoppingCartItems.Add(shoppingCartItem);
                }
            }
            else
            {
                if (product.inventory < shoppingCartItem.Amount + 1)
                {
                    return Content("<script>alert('This product sold all !!!');window.location.reload();</script>");
                }
                else
                {
                    shoppingCartItem.Amount++;
                    shoppingCartItem.Bill = shoppingCartItem.Amount * shoppingCartItem.Price;
                }
            }
            ViewBag.SumOfAmount = GetSumOfAmount();
            ViewBag.SumOfAllBills = GetSumOfAllBills();
            return PartialView("ShoppingCartPartial");
        }






        // update shopping cart item
        [HttpGet]
        public ActionResult UpdateShoppingCart(int id)
        {
            if (Session["shoppingCartItemsByTrieuDD"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            // check exist?? in db
            Product product = allDAO.Products.SingleOrDefault(trieudd => trieudd.id == id);
            if (product == null)
            {
                Response.StatusCode = 400;
                return null;
            }

            // check exist in session
            List<ShoppingCartItem> shoppingCartItems = GetShoppingCart();
            ShoppingCartItem shoppingCartItem = shoppingCartItems.SingleOrDefault(trieudd => trieudd.ProductID == id);
            if (shoppingCartItem == null)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.ShoppingCartItems = shoppingCartItems;

            return View(shoppingCartItem);
        }
        [HttpPost]
        public ActionResult UpdateShoppingCart(ShoppingCartItem shoppingCartItem)
        {
            Product product = allDAO.Products.SingleOrDefault(trieudd => trieudd.id == shoppingCartItem.ProductID);
            if (product.inventory < shoppingCartItem.Amount)
            {
                return View("Notification");
            }


            List<ShoppingCartItem> shoppingCartItems = GetShoppingCart();
            ShoppingCartItem newShoppingCartItem = shoppingCartItems.Find(trieudd => trieudd.ProductID == shoppingCartItem.ProductID);
            newShoppingCartItem.Amount = shoppingCartItem.Amount;
            newShoppingCartItem.Bill = newShoppingCartItem.Price * newShoppingCartItem.Amount;

            return RedirectToAction("WatchShoppingCart");
        }


        // delete shopping cart item
        public ActionResult DeleteShoppingCart(int id)
        {
            if (Session["shoppingCartItemsByTrieuDD"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            Product product = allDAO.Products.SingleOrDefault(trieudd => trieudd.id == id);
            if (product == null)
            {
                Response.StatusCode = 404;
                return null;
            }

            List<ShoppingCartItem> shoppingCartItems = GetShoppingCart();
            ShoppingCartItem shoppingCartItem = shoppingCartItems.SingleOrDefault(trieudd => trieudd.ProductID == id);
            if (shoppingCartItem == null)
            {
                return RedirectToAction("Index", "Home");
            }
            shoppingCartItems.Remove(shoppingCartItem);

            return RedirectToAction("WatchShoppingCart");
        }



        // order shopping cart item
        public ActionResult Order(Customer customer_parameter)
        {
            if (Session["shoppingCartItemsByTrieuDD"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            Customer customer = new Customer();
            if (Session["ssByTrieuDD"] == null)
            {
                // khach van lai
                customer = customer_parameter;
            }
            else
            {
                // khach co tai khoan
                Member member = Session["ssByTrieuDD"] as Member;
                customer.member_id = member.id;

                //   Member member = allDAO.Members.Where(trieudd => trieudd.id == customer.member_id).First();

                customer.name = member.fullname;
                customer.address = member.address;
                customer.email = member.email;
                customer.phone = member.phone;
            }
            allDAO.Customers.Add(customer);
            allDAO.SaveChanges();

            // order
            Order order = new Order();
            order.customer_id = customer.id;
            order.date = DateTime.Now;
            order.delivery_status = false;
            order.is_paid = false;
            order.incentives = 0;
            order.is_cancel = false;
            order.is_delete = false;
            allDAO.Orders.Add(order);
            allDAO.SaveChanges();

            //order detail
            List<ShoppingCartItem> shoppingCartItems = GetShoppingCart();
            foreach (var shoppingCartItem in shoppingCartItems)
            {
                DetailOrder detailOrder = new DetailOrder();
                detailOrder.order_id = order.id;
                detailOrder.product_id = shoppingCartItem.ProductID;
                detailOrder.product_name = shoppingCartItem.ProductName;
                detailOrder.amount = shoppingCartItem.Amount;
                detailOrder.product_price = shoppingCartItem.Price;
                allDAO.DetailOrders.Add(detailOrder);
            }
            allDAO.SaveChanges();

            Session["shoppingCartItemsByTrieuDD"] = null;
            return RedirectToAction("WatchShoppingCart");
        }
    }
}