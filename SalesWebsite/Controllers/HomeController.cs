﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CaptchaMvc.HtmlHelpers;
using SalesWebsite.Models;

namespace SalesWebsite.Controllers
{
    public class HomeController : Controller
    {

        MyUpdateBusinessEntities allDAO = new MyUpdateBusinessEntities();

        public ActionResult Index()
        {
            // listPhone: phone, isNew, !isDelete
            var listPhone = allDAO.Products.Where(trieudd =>
                trieudd.product_type_id == 1
                &&
                trieudd.is_new == true
                &&
                trieudd.is_delete == false
                );
            ViewBag.ListPhone = listPhone;

            // listLaptop: laptop, isNew, !isDelete
            var listLaptop = allDAO.Products.Where(trieudd =>
                 trieudd.product_type_id == 3
                 &&
                 trieudd.is_new == true
                 &&
                 trieudd.is_delete == false
                 );
            ViewBag.ListLaptop = listLaptop;

            // listTablet: tablet, isNew, !isDelete
            var listTablet = allDAO.Products.Where(trieudd =>
                trieudd.product_type_id == 2
                &&
                trieudd.is_new == true
                &&
                trieudd.is_delete == false
                );
            ViewBag.ListTablet = listTablet;

            return View();
        }


        [ChildActionOnly]
        public ActionResult MenuPartial()
        {
            return PartialView(allDAO.Products);
        }


        [HttpGet]
        public ActionResult Register()
        {
            ViewBag.question = new SelectList(LoadSecretQuestions());
            Member member = new Member();
            return View(member);
        }
        [HttpGet]
        public ActionResult RegisterByMS()
        {
            return View(new Member());
        }

        [HttpPost]
        public ActionResult Register(Member member,FormCollection formCollection)
        {
            ViewBag.question = new SelectList(LoadSecretQuestions());
            if (this.IsCaptchaValid("Lỗi rồi bạn ơi !!!"))
            {
                if (ModelState.IsValid)
                {
                    allDAO.Members.Add(member);
                    allDAO.SaveChanges();
                    ViewBag.Msg = "OK, đăng ký thành công";
                }
                else
                {
                    ViewBag.Msg = "Bad register request";
                }
                return View(new Member());
            }
            ViewBag.Msg = "Captcha sai rồi bạn ơi";
            return View(new Member());
        }
        public List<string> LoadSecretQuestions()
        {
            return new List<string>()
            {
                "What is your pet's name ??",
                "Where are your parents meet up first time ??",
                "TrieuDD is the most handsome boy over the world ??"
            };
        }

        public ActionResult Login(FormCollection formCollection)
        {

            string user = formCollection.Get("txtUser");
            string pass = formCollection["txtPass"];
            var member = allDAO.Members.SingleOrDefault(trieudd => trieudd.account.Equals(user) && trieudd.password.Equals(pass));
            if (member != null)
            {
                Session["ssByTrieuDD"] = member;
                return Content("<script>window.location.reload()</script>");
            }
            return Content("Your account or password wrong !!!");
        }

        public ActionResult Logout()
        {
            Session["ssByTrieuDD"] = null;
            return RedirectToAction("Index");
        }
    }
}