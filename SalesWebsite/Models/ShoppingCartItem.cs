﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesWebsite.Models
{
    public class ShoppingCartItem
    {
        public string ProductName { get; set; }
        public int ProductID { set; get; }
        public int Amount { set; get; }
        public decimal Price { get; set; }
        public decimal Bill { get; set; }
        public string Image { get; set; }

        // chac chan truyen vao ma co ton tai --> kiem tra doi tuong tu ma null hay khong o controller
        public ShoppingCartItem(int productID, int amount)
        {
            using (MyUpdateBusinessEntities allDAO = new MyUpdateBusinessEntities())
            {
                this.ProductID = productID;
                Product product = allDAO.Products.Single(trieudd => trieudd.id == productID);
                this.ProductName = product.name;
                this.Image = product.image;
                this.Price = product.price.Value;
                this.Amount = amount;
                this.Bill = this.Price * this.Amount;

            }
        }

        public ShoppingCartItem(int productID)
        {
            using (MyUpdateBusinessEntities allDAO = new MyUpdateBusinessEntities())
            {
                this.ProductID = productID;
                Product product = allDAO.Products.Single(trieudd => trieudd.id == productID);
                this.ProductName = product.name;
                this.Image = product.image;
                this.Price = product.price.Value;
                this.Amount = 1;
                this.Bill = this.Price * this.Amount;

            }
        }

        public ShoppingCartItem()
        {

        }
    }
}