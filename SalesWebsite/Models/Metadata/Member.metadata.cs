﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace SalesWebsite.Models
{
    [MetadataType(typeof(MemberMetadata))]
    public partial class Member
    {
        internal sealed class MemberMetadata
        {
            [Range(0, int.MaxValue, ErrorMessage = "{0} phải từ {1} đến {2}")]
            public int id { get; set; }

            [DisplayName("Account - Tài khoản")]
            [Required(ErrorMessage = "{0} không được bỏ trống")]
            [StringLength(100, ErrorMessage = "{0} qúa {1} ký tự ")]
            public string account { get; set; }

            [DisplayName("Password - Mật khẩu")]
            [Required(ErrorMessage = "{0} không được bỏ trống")]
            [StringLength(100, ErrorMessage = "{0} qúa {1} ký tự ")]
            public string password { get; set; }

            [DisplayName("Fullname - Họ và tên")]
            [Required(ErrorMessage = "{0} không được bỏ trống")]
            [StringLength(100, ErrorMessage = "{0} qúa {1} ký tự ")]
            public string fullname { get; set; }

            [DisplayName("Address - Địa chỉ")]
            [Required(ErrorMessage = "{0} không được bỏ trống")]
            [StringLength(100, ErrorMessage = "{0} qúa {1} ký tự ")]
            public string address { get; set; }

            [DisplayName("Email - Thư điện tử")]
            [Required(ErrorMessage = "{0} không được bỏ trống")]
            [StringLength(100, ErrorMessage = "{0} qúa {1} ký tự ")]
            //        //[RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?
            //", ErrorMessage = "{0} không hợp lệ")]
            public string email { get; set; }

            [DisplayName("Phone number - Số điện thoại")]
            [Required(ErrorMessage = "{0} không được bỏ trống")]
            [StringLength(10, ErrorMessage = "{0} không được quá 10 ký tự")]
            //        //[RegularExpression(@"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}
            //", ErrorMessage = "{0} không hợp lệ")]
            public string phone { get; set; }

            [DisplayName("Secret question - Câu hỏi bí mật")]
            [Required(ErrorMessage = "{0} không được bỏ trống")]
            public string question { get; set; }

            [DisplayName("Answer - Câu trả lời")]
            [Required(ErrorMessage = "{0} không được bỏ trống")]
            [StringLength(100, ErrorMessage = "{0} qúa {1} ký tự ")]
            public string answear { get; set; }

            public Nullable<int> member_type_id { get; set; }

        
        }
    }
}