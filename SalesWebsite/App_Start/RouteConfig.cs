﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SalesWebsite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            // url no parameter
            routes.MapRoute(
          name: "CustomerByTrieuDD",
          url: "customer-trieudd",
          defaults: new { controller = "Customer", action = "Index", id = UrlParameter.Optional }
              );


            // url with parameter
            routes.MapRoute(
           name: "DetailProductByTrieuDD",
           url: "productAt{idd}",
           defaults: new { controller = "Product", action = "DetailProduct", id = UrlParameter.Optional }
       );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );


        }
    }
}
